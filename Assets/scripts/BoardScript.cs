﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BoardScript : MonoBehaviour {

	public TileScript tilePrefab;
	public UnitScript unitPrefab;

	public Board board;

	public Dictionary<Tile, TileScript> tilescripts;
	public Dictionary<Unit, UnitScript> unitscripts;

	void Start(){
		tilescripts = new Dictionary<Tile, TileScript>();
		unitscripts = new Dictionary<Unit, UnitScript>();
	}


	public void SetBoard(Board _board){
		//tilescripts.Clear();

		board = _board;
		Build();
	}

	void Build(){
		foreach(Tile t in board.tiles){
			if(t == null) continue;
			Vector3 position = t.GetPosition();

			//TILE
			TileScript ts = Instantiate(tilePrefab, position, Quaternion.identity) as TileScript;
			ts.boardscript = this;
			ts.tile = t;

			ts.gameObject.name = "Tile_"+t.x+"_"+t.y;
			ts.transform.SetParent(transform);

			tilescripts.Add(t, ts);

			//UNIT
			if(t.unit != null){
				UnitScript us = Instantiate(unitPrefab, position, Quaternion.identity) as UnitScript;
				us.unit = t.unit;

				us.transform.SetParent(transform);

				unitscripts.Add(t.unit, us);
			}

		}
		

	}

	public void ResetBoardColor(){
		foreach(TileScript ts in tilescripts.Values){
			ts.ResetColor();
		}
	}

	public TileScript GetTileScript(Tile t){
		return tilescripts[t];
	}

	public UnitScript GetUnitScript(Unit u){
		return unitscripts[u];
	}
}
