﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UnitScript : MonoBehaviour {

	public Unit unit;

	public MeshRenderer meshRenderer;
	public Text unitTypeLabel;

	// Use this for initialization
	void Start () {
		UpdateUnit();	
	}
	
	// Update is called once per frame
	void Update () {
		Vector3 pos = unit.tile.GetPosition();
		if(transform.position != pos){
			transform.position = Vector3.Lerp(transform.position, pos, Time.deltaTime * 5f);
		}
	}

	public void UpdateUnit(){
		meshRenderer.material.color = unit.player.color;
		unitTypeLabel.text = unit.type.name;
	}
}
