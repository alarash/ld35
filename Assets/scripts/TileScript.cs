﻿using UnityEngine;
using System.Collections;

public class TileScript : MonoBehaviour {

	public BoardScript boardscript;
	public Tile tile;

	MeshRenderer mr;
	Color defaultColor;

	// Use this for initialization
	void Start () {
		mr = GetComponentInChildren<MeshRenderer>();
		defaultColor = mr.material.color;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	/*public void OnClick ()
	{
		Debug.Log("OnClick "+tile);
		//SetColor(Color.white);
		boardscript.OnClickTile(this);
	}*/

	public void SetColor(Color c){
		mr.material.color = c;
	}

	public void ResetColor(){
		SetColor(defaultColor);
	}
}
