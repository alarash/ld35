﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

public class MouseManager : MonoBehaviour {

	GameScript gameScript;

	// Use this for initialization
	void Start () {
		gameScript = GetComponent<GameScript>();
	}
	
	// Update is called once per frame
	void Update () {
		if (EventSystem.current.IsPointerOverGameObject())
			return;

		if(Input.GetMouseButtonDown(0)){
			TileScript ts = null;
			RaycastHit hit;
			if(Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit)){
				ts = hit.collider.GetComponent<TileScript>();
				/*if(ts != null) {
					//ts.OnClick();
					gameScript.OnTileClick(ts);
				}*/
			}

			if(ts != null){
				gameScript.OnTileClick(ts);
			}
			else {
				gameScript.ClearSelection();
			}
		}
	}
}
