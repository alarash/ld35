﻿using System;
using System.Collections.Generic;

public class Unit
{
	public Player player;
	public Tile tile;

	public bool block = true;

	public UnitType type;

	public bool hasMoved = false;
	public bool hasAttacked = false;

	public Unit (Tile _tile)
	{
		tile = _tile;
		_tile.unit = this;

		type = UnitTypeDB.Get("Scissor");
	}

	public List<Tile> GetMoves(){
		//return tile.GetNeighbors(blocked:true);
		if(hasMoved) return new List<Tile>();
		return tile.GetRange(type.movePoint, blocked:true);
	}

	public List<Tile> GetAttacks(){
		List<Tile> tiles = new List<Tile>();
		foreach(Tile t in tile.GetNeighbors()){
			if(t.unit != null && t.unit.player != player){
				tiles.Add(t);
			}
		}
		return tiles;
	}

	public void OnStartTurn(){
		hasMoved = false;
		hasAttacked = false;
	}

	public bool Move(Tile t){
		if(hasMoved) return false;
		if(t.unit != null) return false;

		if(tile != null) tile.unit = null;
		tile = t;
		t.unit = this;

		hasMoved = true;

		return true;
	}

	public void Attack(Tile t){
		Unit u = t.unit;

		hasAttacked = true;
	}


}