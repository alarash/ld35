﻿using System;
using System.Collections.Generic;


public class UnitType
{
	public string name;
	public string description;

	public int movePoint = 0;
	public int attackPoint = 0;

	public UnitType(string name, string description="")
	{
		this.name = name;
		this.description = description;
	}
}

public static class UnitTypeDB
{
	public static Dictionary<string, UnitType> unitTypes;

	static void Clear ()
	{
		UnitTypeDB.unitTypes = null;
	}

	static UnitType AddUnitType (UnitType unitType)
	{
		if(UnitTypeDB.unitTypes == null)
			UnitTypeDB.unitTypes = new Dictionary<string, UnitType>();

		UnitTypeDB.unitTypes.Add(unitType.name, unitType);
		return unitType;
	}

	public static UnitType Get(string name){
		return UnitTypeDB.unitTypes[name];
	}

	public static void Init(){
		UnitTypeDB.Clear();

		UnitType ut;

		//ROCK
		ut = UnitTypeDB.AddUnitType(new UnitType("Rock"));
		ut.movePoint = 1;
		ut.attackPoint = 1;

		//PAPER
		ut = UnitTypeDB.AddUnitType(new UnitType("Paper"));
		ut.movePoint = 2;
		ut.attackPoint = 1;

		//SCISSOR
		ut = UnitTypeDB.AddUnitType(new UnitType("Scissor"));
		ut.movePoint = 3;
		ut.attackPoint = 1;


	}




}