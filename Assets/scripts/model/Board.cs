﻿using System;
using UnityEngine;

public class Board
{
	public int radius;
	public Tile[,] tiles;

	public Board (int _radius = 4)
	{
		radius = _radius;
		tiles = new Tile[radius*2+1, radius*2+1];
		for (int y = 0; y <= radius*2; y++) {
			for (int x = 0; x <= radius*2; x++) {
				Tile t = new Tile(this, x - radius, y - radius);
				if(t.GetPosition().magnitude > radius) continue;
				tiles[x, y] = t;
			}
		}
	}

	public Tile GetTile(int x, int y){
		//Debug.Log("GetTile "+x+" "+y);
		if(x >= -radius && x <= radius && y >= -radius && y <= radius){
			Tile t = tiles[x + radius, y + radius];
			return t;
		}
		return null;

	}
}


