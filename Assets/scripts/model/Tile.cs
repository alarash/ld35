﻿using System;
using UnityEngine;
using System.Collections.Generic;



public class Tile
{
	public Board board;
	public int x, y;

	public Unit unit;

	bool _block = false;
	public bool block {
		get {
			if(_block) return true; 
			if(unit != null) return unit.block;
			return false;
		}
		set {_block = value;}
	}

	public Tile (Board _board, int _x, int _y)
	{
		board = _board;
		x = _x;
		y = _y;
	}

	public Vector3 GetPosition ()
	{
		Vector3 position = new Vector3(x + (0.5f * (y % 2)), 0, y * 0.866f);
		return position;
	}

	public List<Tile> GetNeighbors(bool blocked=false){
		List<Tile> tiles = new List<Tile>();

		Tile t;

		t = board.GetTile(x-1, y);
		if(t != null && (!blocked || !t.block)) tiles.Add(t);

		t = board.GetTile(x+1, y);
		if(t != null && (!blocked || !t.block)) tiles.Add(t);

		t = board.GetTile(x, y+1);
		if(t != null && (!blocked || !t.block)) tiles.Add(t);

		t = board.GetTile(x, y-1);
		if(t != null && (!blocked || !t.block)) tiles.Add(t);

		int xo = (y % 2 == 0 ? 1:-1) * (y+1 <= 0 ? 1:-1);
		t = board.GetTile(x+xo, y+1);
		if(t != null && (!blocked || !t.block)) tiles.Add(t);

		xo = (y % 2 == 0 ? 1:-1) * (y-1 < 0 ? 1:-1);
		t = board.GetTile(x+xo, y-1);
		if(t != null && (!blocked || !t.block)) tiles.Add(t);

		return tiles;
	}

	public List<Tile> GetRange(int range, bool blocked=false){
		List<Tile> tiles = new List<Tile>();

		List<Tile> closedTiles = new List<Tile>();
		closedTiles.Add(this);
		List<Tile> openTiles = new List<Tile>();


		for(int i=0; i < range; i++){
			foreach(Tile t in closedTiles){
				foreach(Tile n in t.GetNeighbors(blocked:blocked)){
					if(closedTiles.Contains(n) || openTiles.Contains(n)) continue;
					tiles.Add(n);
					openTiles.Add(n);
				}
			}

			closedTiles = openTiles;
			openTiles = new List<Tile>();
		}

		return tiles;
	}

	public override string ToString ()
	{
		return "Tile("+x+", "+y+")";
	}
}


