﻿using System;
using UnityEngine;
using System.Collections.Generic;


public class Player
{
	public string name;
	public Color color;

	public List<Unit> units;

	public Player (string _name, Color _color)
	{
		name = _name;
		color = _color;

		units = new List<Unit>();
	}

	public void AddUnit(Unit u){
		units.Add(u);
		u.player = this;
	}

	public void RemoveUnit(Unit u){
		units.Remove(u);
		u.player = null;
	}

	public void OnStartTurn(){
		foreach(Unit u in units){
			u.OnStartTurn();
		}
	}
}


