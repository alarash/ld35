﻿using System;
using UnityEngine;


public class Game
{
	public Board board;

	public Player[] players;
	public int currentPlayerIndex = 0;

	public Game ()
	{
		UnitTypeDB.Init();

		//BOARD
		board = new Board(4);

		//PLAYERS
		players = new Player[2];
		Player p1 = new Player("Player 1", Color.red);
		players[0] = p1;

		Player p2 = new Player("Player 2", Color.blue);
		players[1] = p2;

		//UNITS
		p1.AddUnit(new Unit(board.GetTile(-4, 0)));
		p1.AddUnit(new Unit(board.GetTile(-4, 1)));
		p1.AddUnit(new Unit(board.GetTile(-3, -1)));

		p2.AddUnit(new Unit(board.GetTile(4, 0)));
		p2.AddUnit(new Unit(board.GetTile(3, 1)));
		p2.AddUnit(new Unit(board.GetTile(4, -1)));



	}

	public void EndTurn ()
	{
		currentPlayerIndex = (currentPlayerIndex + 1) % players.Length;
		GetCurrentPlayer().OnStartTurn();
	}

	public Player GetCurrentPlayer ()
	{
		return players[currentPlayerIndex];
	}
}


