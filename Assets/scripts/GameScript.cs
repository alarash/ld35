﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;


public class GameScript : MonoBehaviour {

	Game game;

	BoardScript boardScript;

	//GUI
	public Text playerLabel; 
	public GameObject unitSwitchPanel;

	//UNIT
	Unit selectedUnit;
	List<Tile> moveTiles;
	List<Tile> attackTiles;

	// Use this for initialization
	void Start () {
		game = new Game();

		boardScript = GetComponent<BoardScript>();
		boardScript.SetBoard(game.board);

		UpdatePlayerLabel();

		unitSwitchPanel.SetActive(false);
	}

	public void EndTurn(){
		ClearSelection();
		game.EndTurn();

		UpdatePlayerLabel();
	}

	//PLAYER
	void UpdatePlayerLabel(){
		playerLabel.text = game.GetCurrentPlayer().name;
		playerLabel.color = game.GetCurrentPlayer().color;
	}

	//UNIT
	public void OnTileClick (TileScript ts)
	{
		Tile t = ts.tile;

		//CHECK SELECTED
		if(selectedUnit != null){
			//CHECK ATTACK
			if(attackTiles.Contains(t)){
				selectedUnit.Attack(t);
				UpdateSelectedUnitTiles();
				return;
			}
			else if(moveTiles.Contains(t)){
				selectedUnit.Move(t);
				UpdateSelectedUnitTiles();
				return;
			}
		}

		boardScript.ResetBoardColor();


		ClearSelection();

		if(ts.tile.unit != null){
			SelectUnit(ts);
		}


		//ts.SetColor(Color.red);
	}

	public void SelectUnit(TileScript ts){
		selectedUnit = ts.tile.unit;

		//boardScript.ResetBoardColor();
		//ts.SetColor(Color.white);

		if(selectedUnit.player == game.GetCurrentPlayer()){
			unitSwitchPanel.SetActive(true);
		}

		UpdateSelectedUnitTiles();
	}

	/*
	void ShowUnitMovement (Unit u)
	{
		List<Tile> tiles = u.GetMoves();
		foreach(Tile tile in tiles){
			boardScript.GetTileScript(tile).SetColor(Color.white);
		}
	}

	void ShowUnitAttack (Unit u)
	{
		List<Tile> tiles = u.GetAttacks();
		foreach(Tile tile in tiles){
			boardScript.GetTileScript(tile).SetColor(Color.red);
		}
	}*/

	public void UpdateSelectedUnitTiles(){
		boardScript.ResetBoardColor();
		if(selectedUnit == null) return;

		moveTiles = selectedUnit.GetMoves();
		attackTiles = selectedUnit.GetAttacks();

		//ShowUnitMovement(selectedUnit);
		//ShowUnitAttack(selectedUnit);

		foreach(Tile tile in moveTiles){
			boardScript.GetTileScript(tile).SetColor(Color.white);
		}

		foreach(Tile tile in attackTiles){
			boardScript.GetTileScript(tile).SetColor(Color.red);
		}
	}

	public void SwitchSelectedUnitType(string typename){
		if(selectedUnit == null) return;

		selectedUnit.type = UnitTypeDB.Get(typename);

		boardScript.GetUnitScript(selectedUnit).UpdateUnit();
		UpdateSelectedUnitTiles();
	}

	//Clear
	public void ClearSelection ()
	{
		//boardScript.ResetBoardColor();
		selectedUnit = null;
		UpdateSelectedUnitTiles();

		unitSwitchPanel.SetActive(false);
	}
}
